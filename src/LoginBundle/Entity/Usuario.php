<?php

namespace LoginBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Usuario
 */
class Usuario implements UserInterface
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $nombre;

    /**
     * @var string
     */
    private $apellidop;

    /**
     * @var string
     */
    private $apellidom;

    /**
     * @var string
     */
    private $puesto;

    /**
     * @var string
     */
    private $usuario;

    /**
     * @var string
     */
    private $password;

    /**
     * @var \DateTime
     */
    private $fechahabilitacion;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Usuario
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set apellidop
     *
     * @param string $apellidop
     *
     * @return Usuario
     */
    public function setApellidop($apellidop)
    {
        $this->apellidop = $apellidop;

        return $this;
    }

    /**
     * Get apellidop
     *
     * @return string
     */
    public function getApellidop()
    {
        return $this->apellidop;
    }

    /**
     * Set apellidom
     *
     * @param string $apellidom
     *
     * @return Usuario
     */
    public function setApellidom($apellidom)
    {
        $this->apellidom = $apellidom;

        return $this;
    }

    /**
     * Get apellidom
     *
     * @return string
     */
    public function getApellidom()
    {
        return $this->apellidom;
    }

    /**
     * Set puesto
     *
     * @param string $puesto
     *
     * @return Usuario
     */
    public function setPuesto($puesto)
    {
        $this->puesto = $puesto;

        return $this;
    }

    /**
     * Get puesto
     *
     * @return string
     */
    public function getPuesto()
    {
        return $this->puesto;
    }

    /**
     * Set usuario
     *
     * @param string $usuario
     *
     * @return Usuario
     */
    public function setUsuario($usuario)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * Get usuario
     *
     * @return string
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return Usuario
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set fechahabilitacion
     *
     * @param \DateTime $fechahabilitacion
     *
     * @return Usuario
     */
    public function setFechahabilitacion($fechahabilitacion)
    {
        $this->fechahabilitacion = $fechahabilitacion;

        return $this;
    }

    /**
     * Get fechahabilitacion
     *
     * @return \DateTime
     */
    public function getFechahabilitacion()
    {
        return $this->fechahabilitacion;
    }
    
    public function getRoles()
    {
        //print_r($this->rol);die();
        return array($this->rol);
    }

    public function eraseCredentials()
    {
    }

    /** @see \Serializable::serialize() */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->usuario,
            $this->password,
            // see section on salt below
            // $this->salt,
        ));
    }

    /** @see \Serializable::unserialize() */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->usuario,
            $this->password,
            // see section on salt below
            // $this->salt
        ) = unserialize($serialized);
    }
    
    public function getSalt()
    {
        // The bcrypt algorithm don't require a separate salt.
        // You *may* need a real salt if you choose a different encoder.
        return null;
    }
    
    public function getUsername()
    {
        return $this->usuario;
    }

    public function setUsername($username)
    {
        $this->usuario = $username;
    }
    /**
     * @var string
     */
    private $rol;


    /**
     * Set rol
     *
     * @param string $rol
     *
     * @return Usuario
     */
    public function setRol($rol)
    {
        $this->rol = $rol;

        return $this;
    }

    /**
     * Get rol
     *
     * @return string
     */
    public function getRol()
    {
        return $this->rol;
    }
    /**
     * @var string
     */
    private $foto;


    /**
     * Set foto
     *
     * @param string $foto
     *
     * @return Usuario
     */
    public function setFoto($foto)
    {
        $this->foto = $foto;

        return $this;
    }

    /**
     * Get foto
     *
     * @return string
     */
    public function getFoto()
    {
        return $this->foto;
    }
    /**
     * @var string
     */
    private $correo;


    /**
     * Set correo
     *
     * @param string $correo
     *
     * @return Usuario
     */
    public function setCorreo($correo)
    {
        $this->correo = $correo;

        return $this;
    }

    /**
     * Get correo
     *
     * @return string
     */
    public function getCorreo()
    {
        return $this->correo;
    }
    /**
     * @var integer
     */
    private $telefono;

    /**
     * @var string
     */
    private $rfc;

    /**
     * @var string
     */
    private $calle;

    /**
     * @var string
     */
    private $numint;

    /**
     * @var string
     */
    private $numext;

    /**
     * @var string
     */
    private $colonia;

    /**
     * @var integer
     */
    private $cp;


    /**
     * Set telefono
     *
     * @param integer $telefono
     *
     * @return Usuario
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;

        return $this;
    }

    /**
     * Get telefono
     *
     * @return integer
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * Set rfc
     *
     * @param string $rfc
     *
     * @return Usuario
     */
    public function setRfc($rfc)
    {
        $this->rfc = $rfc;

        return $this;
    }

    /**
     * Get rfc
     *
     * @return string
     */
    public function getRfc()
    {
        return $this->rfc;
    }

    /**
     * Set calle
     *
     * @param string $calle
     *
     * @return Usuario
     */
    public function setCalle($calle)
    {
        $this->calle = $calle;

        return $this;
    }

    /**
     * Get calle
     *
     * @return string
     */
    public function getCalle()
    {
        return $this->calle;
    }

    /**
     * Set numint
     *
     * @param string $numint
     *
     * @return Usuario
     */
    public function setNumint($numint)
    {
        $this->numint = $numint;

        return $this;
    }

    /**
     * Get numint
     *
     * @return string
     */
    public function getNumint()
    {
        return $this->numint;
    }

    /**
     * Set numext
     *
     * @param string $numext
     *
     * @return Usuario
     */
    public function setNumext($numext)
    {
        $this->numext = $numext;

        return $this;
    }

    /**
     * Get numext
     *
     * @return string
     */
    public function getNumext()
    {
        return $this->numext;
    }

    /**
     * Set colonia
     *
     * @param string $colonia
     *
     * @return Usuario
     */
    public function setColonia($colonia)
    {
        $this->colonia = $colonia;

        return $this;
    }

    /**
     * Get colonia
     *
     * @return string
     */
    public function getColonia()
    {
        return $this->colonia;
    }

    /**
     * Set cp
     *
     * @param integer $cp
     *
     * @return Usuario
     */
    public function setCp($cp)
    {
        $this->cp = $cp;

        return $this;
    }

    /**
     * Get cp
     *
     * @return integer
     */
    public function getCp()
    {
        return $this->cp;
    }
}
