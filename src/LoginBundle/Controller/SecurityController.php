<?php

// src/LoginBundle/Controller/SecurityController.php

namespace LoginBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class SecurityController extends Controller {

    public function loginAction(Request $request) {
      
        $authenticationUtils = $this->get('security.authentication_utils');

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();

        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        if ($error) {
            $error = 'Crendeciales no válidas';
            $this->get('session')->getFlashBag()->add(
                    'warning', $error
            );
        }
        return $this->render('LoginBundle:security:login.html.twig', array('last_username' => $lastUsername, 'error' => $error,)
        );
    }

    public function checkAction() {

        $user = $this->container->get('security.context')->getToken()->getUser();
        
        if ($user == "anon.") {
            
            return $this->redirect($this->generateUrl('front_homepage'));
        } else {
            //print_r($user->getId());die();
            //$pacientesde = $em->getRepository('LoginBundle:Usuario')->();
            switch ($user->getRol()) {
                case 'ROLE_ADMIN':
                    return $this->redirect($this->generateUrl('admin'));
                    break;
                case 'ROLE_USER':
                    return $this->redirect($this->generateUrl('admin'));
                    break;
            }
        }
    }

}
