<?php

namespace BackBundle\Entity;

/**
 * Paquetes
 */
class Paquetes
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $nombre;

    /**
     * @var int
     */
    private $fotos;

    /**
     * @var int
     */
    private $categorias;

    /**
     * @var float
     */
    private $costo;

    /**
     * @var bool
     */
    private $mapa;

    /**
     * @var bool
     */
    private $contactodirecto;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Paquetes
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set fotos
     *
     * @param integer $fotos
     *
     * @return Paquetes
     */
    public function setFotos($fotos)
    {
        $this->fotos = $fotos;

        return $this;
    }

    /**
     * Get fotos
     *
     * @return int
     */
    public function getFotos()
    {
        return $this->fotos;
    }

    /**
     * Set categorias
     *
     * @param integer $categorias
     *
     * @return Paquetes
     */
    public function setCategorias($categorias)
    {
        $this->categorias = $categorias;

        return $this;
    }

    /**
     * Get categorias
     *
     * @return int
     */
    public function getCategorias()
    {
        return $this->categorias;
    }

    /**
     * Set costo
     *
     * @param float $costo
     *
     * @return Paquetes
     */
    public function setCosto($costo)
    {
        $this->costo = $costo;

        return $this;
    }

    /**
     * Get costo
     *
     * @return float
     */
    public function getCosto()
    {
        return $this->costo;
    }

    /**
     * Set mapa
     *
     * @param boolean $mapa
     *
     * @return Paquetes
     */
    public function setMapa($mapa)
    {
        $this->mapa = $mapa;

        return $this;
    }

    /**
     * Get mapa
     *
     * @return bool
     */
    public function getMapa()
    {
        return $this->mapa;
    }

    /**
     * Set contactodirecto
     *
     * @param boolean $contactodirecto
     *
     * @return Paquetes
     */
    public function setContactodirecto($contactodirecto)
    {
        $this->contactodirecto = $contactodirecto;

        return $this;
    }

    /**
     * Get contactodirecto
     *
     * @return bool
     */
    public function getContactodirecto()
    {
        return $this->contactodirecto;
    }
}
