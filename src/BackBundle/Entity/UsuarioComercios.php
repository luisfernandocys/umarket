<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace BackBundle\Entity;

/**
 * Description of UsuarioComercios
 *
 * @author Herson Mancera
 */
class UsuarioComercios {
    //put your code here
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $comentarios;

    /**
     * @var \LoginBundle\Entity\Usuario
     */
    private $usuario;

    /**
     * @var \BackBundle\Entity\Comercios
     */
    private $comercio;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set comentarios
     *
     * @param string $comentarios
     *
     * @return UsuarioComercios
     */
    public function setComentarios($comentarios)
    {
        $this->comentarios = $comentarios;

        return $this;
    }

    /**
     * Get comentarios
     *
     * @return string
     */
    public function getComentarios()
    {
        return $this->comentarios;
    }

    /**
     * Set usuario
     *
     * @param \LoginBundle\Entity\Usuario $usuario
     *
     * @return UsuarioComercios
     */
    public function setUsuario(\LoginBundle\Entity\Usuario $usuario = null)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * Get usuario
     *
     * @return \LoginBundle\Entity\Usuario
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * Set comercio
     *
     * @param \BackBundle\Entity\Comercios $comercio
     *
     * @return UsuarioComercios
     */
    public function setComercio(\BackBundle\Entity\Comercios $comercio = null)
    {
        $this->comercio = $comercio;

        return $this;
    }

    /**
     * Get comercio
     *
     * @return \BackBundle\Entity\Comercios
     */
    public function getComercio()
    {
        return $this->comercio;
    }
}
