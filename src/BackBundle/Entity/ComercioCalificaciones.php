<?php

namespace BackBundle\Entity;

/**
 * ComercioCalificaciones
 */
class ComercioCalificaciones
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $calificacion;

    /**
     * @var string
     */
    private $comentarios;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set calificacion
     *
     * @param integer $calificacion
     *
     * @return ComercioCalificaciones
     */
    public function setCalificacion($calificacion)
    {
        $this->calificacion = $calificacion;

        return $this;
    }

    /**
     * Get calificacion
     *
     * @return int
     */
    public function getCalificacion()
    {
        return $this->calificacion;
    }

    /**
     * Set comentarios
     *
     * @param string $comentarios
     *
     * @return ComercioCalificaciones
     */
    public function setComentarios($comentarios)
    {
        $this->comentarios = $comentarios;

        return $this;
    }

    /**
     * Get comentarios
     *
     * @return string
     */
    public function getComentarios()
    {
        return $this->comentarios;
    }
    /**
     * @var \LoginBundle\Entity\Usuario
     */
    private $usuario;

    /**
     * @var \BackBundle\Entity\Comercios
     */
    private $comercio;


    /**
     * Set usuario
     *
     * @param \LoginBundle\Entity\Usuario $usuario
     *
     * @return ComercioCalificaciones
     */
    public function setUsuario(\LoginBundle\Entity\Usuario $usuario = null)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * Get usuario
     *
     * @return \LoginBundle\Entity\Usuario
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * Set comercio
     *
     * @param \BackBundle\Entity\Comercios $comercio
     *
     * @return ComercioCalificaciones
     */
    public function setComercio(\BackBundle\Entity\Comercios $comercio = null)
    {
        $this->comercio = $comercio;

        return $this;
    }

    /**
     * Get comercio
     *
     * @return \BackBundle\Entity\Comercios
     */
    public function getComercio()
    {
        return $this->comercio;
    }
}
