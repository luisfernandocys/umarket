<?php

namespace BackBundle\Entity;

/**
 * Subcategorias
 */
class Subcategorias
{
    /**
     * @var int
     */
    private $id;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * @var \BackBundle\Entity\Categorias
     */
    private $categoria;


    /**
     * Set categoria
     *
     * @param \BackBundle\Entity\Categorias $categoria
     *
     * @return Subcategorias
     */
    public function setCategoria(\BackBundle\Entity\Categorias $categoria = null)
    {
        $this->categoria = $categoria;

        return $this;
    }

    /**
     * Get categoria
     *
     * @return \BackBundle\Entity\Categorias
     */
    public function getCategoria()
    {
        return $this->categoria;
    }
}
