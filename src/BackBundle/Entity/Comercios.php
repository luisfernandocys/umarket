<?php

namespace BackBundle\Entity;

/**
 * Comercios
 */
class Comercios
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $string;

    /**
     * @var string
     */
    private $ruta;

    /**
     * @var string
     */
    private $contacto;

    /**
     * @var string
     */
    private $nombreComercial;

    /**
     * @var string
     */
    private $direccion;

    /**
     * @var string
     */
    private $telefono;

    /**
     * @var string
     */
    private $ciudad;

    /**
     * @var string
     */
    private $colonia;

    /**
     * @var string
     */
    private $estado;

    /**
     * @var string
     */
    private $cp;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $maps;

    /**
     * @var float
     */
    private $promedio;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set string
     *
     * @param string $string
     *
     * @return Comercios
     */
    public function setString($string)
    {
        $this->string = $string;

        return $this;
    }

    /**
     * Get string
     *
     * @return string
     */
    public function getString()
    {
        return $this->string;
    }

    /**
     * Set ruta
     *
     * @param string $ruta
     *
     * @return Comercios
     */
    public function setRuta($ruta)
    {
        $this->ruta = $ruta;

        return $this;
    }

    /**
     * Get ruta
     *
     * @return string
     */
    public function getRuta()
    {
        return $this->ruta;
    }

    /**
     * Set contacto
     *
     * @param string $contacto
     *
     * @return Comercios
     */
    public function setContacto($contacto)
    {
        $this->contacto = $contacto;

        return $this;
    }

    /**
     * Get contacto
     *
     * @return string
     */
    public function getContacto()
    {
        return $this->contacto;
    }

    /**
     * Set nombreComercial
     *
     * @param string $nombreComercial
     *
     * @return Comercios
     */
    public function setNombreComercial($nombreComercial)
    {
        $this->nombreComercial = $nombreComercial;

        return $this;
    }

    /**
     * Get nombreComercial
     *
     * @return string
     */
    public function getNombreComercial()
    {
        return $this->nombreComercial;
    }

    /**
     * Set direccion
     *
     * @param string $direccion
     *
     * @return Comercios
     */
    public function setDireccion($direccion)
    {
        $this->direccion = $direccion;

        return $this;
    }

    /**
     * Get direccion
     *
     * @return string
     */
    public function getDireccion()
    {
        return $this->direccion;
    }

    /**
     * Set telefono
     *
     * @param string $telefono
     *
     * @return Comercios
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;

        return $this;
    }

    /**
     * Get telefono
     *
     * @return string
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * Set ciudad
     *
     * @param string $ciudad
     *
     * @return Comercios
     */
    public function setCiudad($ciudad)
    {
        $this->ciudad = $ciudad;

        return $this;
    }

    /**
     * Get ciudad
     *
     * @return string
     */
    public function getCiudad()
    {
        return $this->ciudad;
    }

    /**
     * Set colonia
     *
     * @param string $colonia
     *
     * @return Comercios
     */
    public function setColonia($colonia)
    {
        $this->colonia = $colonia;

        return $this;
    }

    /**
     * Get colonia
     *
     * @return string
     */
    public function getColonia()
    {
        return $this->colonia;
    }

    /**
     * Set estado
     *
     * @param string $estado
     *
     * @return Comercios
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set cp
     *
     * @param string $cp
     *
     * @return Comercios
     */
    public function setCp($cp)
    {
        $this->cp = $cp;

        return $this;
    }

    /**
     * Get cp
     *
     * @return string
     */
    public function getCp()
    {
        return $this->cp;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Comercios
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set maps
     *
     * @param string $maps
     *
     * @return Comercios
     */
    public function setMaps($maps)
    {
        $this->maps = $maps;

        return $this;
    }

    /**
     * Get maps
     *
     * @return string
     */
    public function getMaps()
    {
        return $this->maps;
    }

    /**
     * Set promedio
     *
     * @param float $promedio
     *
     * @return Comercios
     */
    public function setPromedio($promedio)
    {
        $this->promedio = $promedio;

        return $this;
    }

    /**
     * Get promedio
     *
     * @return float
     */
    public function getPromedio()
    {
        return $this->promedio;
    }
    /**
     * @var \BackBundle\Entity\Status
     */
    private $status;

    /**
     * @var \BackBundle\Entity\Paquetes
     */
    private $paquete;


    /**
     * Set status
     *
     * @param \BackBundle\Entity\Status $status
     *
     * @return Comercios
     */
    public function setStatus(\BackBundle\Entity\Status $status = null)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return \BackBundle\Entity\Status
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set paquete
     *
     * @param \BackBundle\Entity\Paquetes $paquete
     *
     * @return Comercios
     */
    public function setPaquete(\BackBundle\Entity\Paquetes $paquete = null)
    {
        $this->paquete = $paquete;

        return $this;
    }

    /**
     * Get paquete
     *
     * @return \BackBundle\Entity\Paquetes
     */
    public function getPaquete()
    {
        return $this->paquete;
    }
}
