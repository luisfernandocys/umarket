<?php

namespace BackBundle\Entity;

/**
 * ProductoFotos
 */
class ProductoFotos
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $imagen;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set imagen
     *
     * @param string $imagen
     *
     * @return ProductoFotos
     */
    public function setImagen($imagen)
    {
        $this->imagen = $imagen;

        return $this;
    }

    /**
     * Get imagen
     *
     * @return string
     */
    public function getImagen()
    {
        return $this->imagen;
    }
    /**
     * @var \BackBundle\Entity\Productos
     */
    private $producto;


    /**
     * Set producto
     *
     * @param \BackBundle\Entity\Productos $producto
     *
     * @return ProductoFotos
     */
    public function setProducto(\BackBundle\Entity\Productos $producto = null)
    {
        $this->producto = $producto;

        return $this;
    }

    /**
     * Get producto
     *
     * @return \BackBundle\Entity\Productos
     */
    public function getProducto()
    {
        return $this->producto;
    }
}
