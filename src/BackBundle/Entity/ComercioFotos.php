<?php

namespace BackBundle\Entity;

/**
 * ComercioFotos
 */
class ComercioFotos
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $ruta;

    /**
     * @var string
     */
    private $comentarios;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ruta
     *
     * @param string $ruta
     *
     * @return ComercioFotos
     */
    public function setRuta($ruta)
    {
        $this->ruta = $ruta;

        return $this;
    }

    /**
     * Get ruta
     *
     * @return string
     */
    public function getRuta()
    {
        return $this->ruta;
    }

    /**
     * Set comentarios
     *
     * @param string $comentarios
     *
     * @return ComercioFotos
     */
    public function setComentarios($comentarios)
    {
        $this->comentarios = $comentarios;

        return $this;
    }

    /**
     * Get comentarios
     *
     * @return string
     */
    public function getComentarios()
    {
        return $this->comentarios;
    }
    /**
     * @var \BackBundle\Entity\Comercios
     */
    private $comercio;


    /**
     * Set comercio
     *
     * @param \BackBundle\Entity\Comercios $comercio
     *
     * @return ComercioFotos
     */
    public function setComercio(\BackBundle\Entity\Comercios $comercio = null)
    {
        $this->comercio = $comercio;

        return $this;
    }

    /**
     * Get comercio
     *
     * @return \BackBundle\Entity\Comercios
     */
    public function getComercio()
    {
        return $this->comercio;
    }
}
