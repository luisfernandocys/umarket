<?php

namespace BackBundle\Entity;

/**
 * Productos
 */
class Productos
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $nombre;

    /**
     * @var string
     */
    private $descripcion;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Productos
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return Productos
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }
    /**
     * @var \BackBundle\Entity\Comercios
     */
    private $comercio;


    /**
     * Set comercio
     *
     * @param \BackBundle\Entity\Comercios $comercio
     *
     * @return Productos
     */
    public function setComercio(\BackBundle\Entity\Comercios $comercio = null)
    {
        $this->comercio = $comercio;

        return $this;
    }

    /**
     * Get comercio
     *
     * @return \BackBundle\Entity\Comercios
     */
    public function getComercio()
    {
        return $this->comercio;
    }
}
