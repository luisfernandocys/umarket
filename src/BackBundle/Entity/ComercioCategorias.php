<?php

namespace BackBundle\Entity;

/**
 * ComercioCategorias
 */
class ComercioCategorias
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $comentarios;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set comentarios
     *
     * @param string $comentarios
     *
     * @return ComercioCategorias
     */
    public function setComentarios($comentarios)
    {
        $this->comentarios = $comentarios;

        return $this;
    }

    /**
     * Get comentarios
     *
     * @return string
     */
    public function getComentarios()
    {
        return $this->comentarios;
    }
    /**
     * @var \BackBundle\Entity\Subcategorias
     */
    private $subcategoria;

    /**
     * @var \BackBundle\Entity\Comercios
     */
    private $comercio;


    /**
     * Set subcategoria
     *
     * @param \BackBundle\Entity\Subcategorias $subcategoria
     *
     * @return ComercioCategorias
     */
    public function setSubcategoria(\BackBundle\Entity\Subcategorias $subcategoria = null)
    {
        $this->subcategoria = $subcategoria;

        return $this;
    }

    /**
     * Get subcategoria
     *
     * @return \BackBundle\Entity\Subcategorias
     */
    public function getSubcategoria()
    {
        return $this->subcategoria;
    }

    /**
     * Set comercio
     *
     * @param \BackBundle\Entity\Comercios $comercio
     *
     * @return ComercioCategorias
     */
    public function setComercio(\BackBundle\Entity\Comercios $comercio = null)
    {
        $this->comercio = $comercio;

        return $this;
    }

    /**
     * Get comercio
     *
     * @return \BackBundle\Entity\Comercios
     */
    public function getComercio()
    {
        return $this->comercio;
    }
}
